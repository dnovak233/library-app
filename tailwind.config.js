module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/common/**/*.{js,ts,jsx,tsx}",
    "./src/modules/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    spacing: {
      '0.25': '.25rem',
      '0.5': '.5rem',
      '1': '1rem',
      '2': '2rem',
      '3': '3rem',
      '4': '4rem',
      '5': '5rem',
      '6': '6rem',
      '7': '7rem',
      '8': '8rem',
      '9': '9rem',
      '10': '10rem',
      '16': '16rem',
      '32': '32rem',
    },
    extend: {
      fontFamily: {
        'sans': ['TTNormsPro', 'sans-serif'],
      },
      colors: {
        'white': '#fdfdfd',
        'gray': '#efefef',
        'dark-gray': '#5f6a83',
        'green': '#2b839b',
        'red': '#e17979',
      },
    },
  },
  plugins: [],
}
