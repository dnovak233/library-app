import {Book, BookSlim, Persisted, SqlUser, SqlUserBook} from '@common/models';
import {MockDB} from '@server/db/MockDB';
import {v4 as uuidv4} from 'uuid';

export const getBorrowedQuantity = (id: string): number => {
  const borrowedBooksMap = new Map<string, number>();
  MockDB.users.forEach(({ books }: SqlUser) => {
    books.forEach(({ bookId, quantity}: SqlUserBook) => {
      const currentValue = borrowedBooksMap.get(bookId) || 0;
      borrowedBooksMap.set(bookId, currentValue + quantity);
    })
  });
  return borrowedBooksMap.get(id) || 0;
}

export const getBooks = (): Persisted<BookSlim>[] => {
  return MockDB.books.map(({ id, title, author, quantity}) => {
    return { id, title, author, quantity, available: quantity - getBorrowedQuantity(id) }
  })
}

export const getBook = (id: string): Persisted<Book> | null => {
  const book = MockDB.books.find((b: Persisted<Book>) => b.id === id);
  if (!book) return null;

  const borrowedQuantity = getBorrowedQuantity(id);
  return { ...book, available: book.quantity - borrowedQuantity };
}

export const createBook = (book: Book): Persisted<Book> => {
  const newBook = {
    id: uuidv4(),
    ...book
  };

  MockDB.books.push(newBook);

  return newBook;
}

export const updateBook = (book: Persisted<Book>, newBook: Book): Persisted<Book> => {
  let index = MockDB.books.indexOf(book);
  const updatedBook = Object.assign({ id: book.id }, newBook);
  MockDB.books[index] = updatedBook;
  return updatedBook;
}
