import {BookSlim, Persisted, SqlUser, SqlUserBook, User} from '@common/models';
import {MockDB} from '@server/db/MockDB';
import {v4 as uuidv4} from 'uuid';
import {getBook} from '@server/repository/bookRepo';

export const getUsers = (): Persisted<User>[] => {
  return MockDB.users.map(({ id, firstName, lastName, dob }: SqlUser) => ({ id, firstName, lastName, dob }));
}

export const getUser = (id: string): SqlUser | null => {
  return MockDB.users.find((u: SqlUser) => u.id === id) || null;
}

export const createUser = (user: User): Persisted<User> => {
  const newSqlUser: SqlUser = {
    id: uuidv4(),
    books: [],
    ...user,
  };

  MockDB.users.push(newSqlUser);
  const { id, firstName, lastName, dob } = newSqlUser;
  return { id, firstName, lastName, dob };
}

export const updateUser = (oldUser: SqlUser, newUser: User): Persisted<User> => {
  let index = MockDB.users.indexOf(oldUser);
  const updatedUser: SqlUser = Object.assign({ id: oldUser.id, books: oldUser.books }, newUser);
  MockDB.users[index] = updatedUser;
  const { id, firstName, lastName, dob } = updatedUser;
  return { id, firstName, lastName, dob }
}

export const getUserBooks = (sqlUser: SqlUser): Persisted<BookSlim>[] => {
  return sqlUser.books.map(({ quantity, bookId }: SqlUserBook) => {
    const { id, title, author } = getBook(bookId)!;
    return {
      id,
      title,
      author,
      quantity,
    }
  });
}

export const addUserBook = (sqlUser: SqlUser, bookId: string): boolean => {
  const borrowedBook = sqlUser.books.find((b: SqlUserBook) => b.bookId === bookId);
  if (borrowedBook) {
    borrowedBook.quantity++;
    return true;
  }

  sqlUser.books = [...sqlUser.books, { bookId, quantity: 1}];
  return true;
}

export const removeUserBook = (sqlUser: SqlUser, bookId: string): boolean => {
  const borrowedBook = sqlUser.books.find((b: SqlUserBook) => b.bookId === bookId);
  if (borrowedBook) {
    if (borrowedBook.quantity > 1) {
      borrowedBook.quantity--;
      return true;
    } else {
      sqlUser.books = sqlUser.books.filter((b: SqlUserBook) => b.bookId !== bookId);
      return true;
    }
  }
  return false;
}
