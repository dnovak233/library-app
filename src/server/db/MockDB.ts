import {Book, Persisted, SqlUser} from '@common/models';

const BOOKS: Persisted<Book>[] = [
  {
    id: '1',
    title: 'Harry Potter and the Philosopher\'s Stone',
    author: 'J.K. Rowling',
    quantity: 2,
    summary: 'It is a story about Harry Potter, an orphan brought up by his aunt and uncle because his parents were killed when he was a baby. Harry is unloved by his uncle and aunt but everything changes when he is invited to join Hogwarts School of Witchcraft and Wizardry and he finds out he\'s a wizard.',
  },
  {
    id: '2',
    title: 'The Lord of the Rings: The Fellowship of the Ring',
    author: 'J.R.R. Tolkein',
    quantity: 3,
    summary: 'In the Second Age of Middle-earth, the lords of Elves, Dwarves, and Men are given Rings of Power. Unbeknownst to them, the Dark Lord Sauron forges the One Ring in Mount Doom, instilling into it a great part of his power, in order to dominate the other Rings so he might conquer Middle-earth.',
  },
  {
    id: '3',
    title: 'A Game of Thrones: A Song of Ice and Fire',
    author: 'George R. R. Martin',
    quantity: 5,
    summary: 'A Game of Thrones takes place over the course of one year on or near the fictional continent of Westeros. The story begins when King Robert visits the northern castle Winterfell to ask Ned Stark to be his right-hand assistant, or Hand of the King. The previous Hand, Jon Arryn, died under suspicious circumstances.',
  },
  {
    id: '4',
    quantity: 7,
    title: 'Harry Potter and the Chamber of Secrets',
    summary: 'The plot follows Harry\'s second year at Hogwarts School of Witchcraft and Wizardry, during which a series of messages on the walls of the school\'s corridors warn that the "Chamber of Secrets" has been opened and that the "heir of Slytherin" would kill all pupils who do not come from all-magical families.',
    author: 'J.K. Rowling',
  }
];

const USERS: SqlUser[] = [
  {
    id: '1',
    firstName: 'Dan',
    lastName: 'Novak',
    dob: '1993-03-23',
    books: [
      { bookId: '1', quantity: 1 },
      { bookId: '2', quantity: 1 }
    ],
  },
  {
    id: '2',
    firstName: 'Leon',
    lastName: 'Rotim',
    dob: '1993-01-11',
    books: [
      { bookId: '1', quantity: 1 },
      { bookId: '3', quantity: 1 },
    ],
  },
]

export const MockDB = {
  books: BOOKS,
  users: USERS,
}

