import {useCallback} from 'react';
import {useAsyncData} from '@common/hooks';
import {Persisted, User} from '@common/models';
import {UserAPI} from '@modules/users/api';

export function useUserGet(id: string) {
  const getUser = useCallback(
    async () => UserAPI.getUser(id),
    [id]
  );

  const { data, error, loading } = useAsyncData<Persisted<User> | undefined>(getUser);

  return {
    loading,
    error,
    user: data,
  }
}
