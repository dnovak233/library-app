import {useCallback} from 'react';
import {UserAPI} from '@modules/users/api';
import {useAsyncData} from '@common/hooks';
import {BookSlim, Persisted} from '@common/models';

export function useUserBooksGet(id: string) {
  const getUserBooks = useCallback(
    async () => UserAPI.getBooks(id),
    [id],
  );

  const {
    data: books,
    setData: setBooks,
    reloadData: reloadBooks,
    error,
    loading
  } = useAsyncData<Persisted<BookSlim>[] | undefined>(getUserBooks);

  return {
    loading,
    error,
    books,
    setBooks,
    reloadBooks,
  }
}
