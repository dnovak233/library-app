import {User, Book} from '@common/models';
import {Persisted} from '@common/models';
import axios from 'axios';

const api = axios.create({
  baseURL: '/api/users',
  headers: { 'Content-Type': 'application/json' }
});

export const UserAPI = {
  getUsers: async (): Promise<Persisted<User>[]> => {
    return await api.get('/').then(res => res.data);
  },
  getUser: async (id: string): Promise<Persisted<User>> => {
    return await api.get(`/${id}`).then(res => res.data);
  },
  postUser: async (user: User): Promise<Persisted<User>> => {
    return await api.post('/', user).then(res => res.data);
  },
  putUser: async (id: string, user: User): Promise<Persisted<User>> => {
    return await api.put(`/${id}`, user).then(res => res.data);
  },
  getBooks: async (id:string): Promise<Persisted<Book>[]> => {
    return await api.get(`/${id}/books`).then(res => res.data);
  },
  postBook: async (id:string, bookId: string): Promise<Persisted<Book>> => {
    return await api.post(`/${id}/books/${bookId}`).then(res => res.data);
  },
  deleteBook: async (id:string, bookId: string): Promise<void> => {
    return await api.delete(`/${id}/books/${bookId}`).then(res => res.data);
  }
}
