import {FC} from 'react';
import {Persisted, User} from '@common/models';
import Link from 'next/link';
import differenceInYears from 'date-fns/differenceInYears';

interface Props {
  user: Persisted<User>;
}

const UserCard: FC<Props> = ({ user: { firstName, lastName, dob, id } }) => {
  const age = differenceInYears(new Date(), new Date(dob));

  return (
    <div className="card flex">
      <div>
        <h3>{`${firstName} ${lastName}`}</h3>
        <p>Age: {age}</p>
      </div>
      <div className="flex items-center ml-auto">
        <Link href={`/users/${id}`}>
          <a className="mx-0.5">View</a>
        </Link>
        <Link href={`/users/${id}/edit`}>
          <a className="mx-0.5">Edit</a>
        </Link>
      </div>
    </div>
  );
}
export default UserCard;
