import Head from 'next/head';
import ViewLoader from '@common/components/ViewLoader/ViewLoader';
import differenceInYears from 'date-fns/differenceInYears';
import {FC} from 'react';
import {useUserGet} from '@modules/users/hooks/useUserGet';

interface Props {
  id: string;
}

const UserView: FC<Props> = ({ id }) => {
  const { user, loading, error } = useUserGet(id);

  const fullName = user ? `${user.firstName} ${user.lastName}` : '';
  const age = user ? differenceInYears(new Date(), new Date(user.dob)) : '';

  return (
    <>
      <Head>
        <title>{user ? `Library App | ${fullName}` : 'Library App'}</title>
        <meta name="description" content="Book details" />
      </Head>

      <div className="box__fixed">
        <ViewLoader loaded={!loading}>
          {error && <p>Error!</p>}
          {user && (
            <div>
              <h1>{fullName}</h1>
              <p>Age: {age}</p>
            </div>
          )}
        </ViewLoader>
      </div>
    </>
  );
}
export default UserView;
