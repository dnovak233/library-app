import {FC, useState} from 'react';
import ViewLoader from '@common/components/ViewLoader/ViewLoader';
import {useUserBooksGet} from '@modules/users/hooks';
import {BookSlim, Persisted} from '@common/models';
import BookCard from '@common/components/BookCard/BookCard';
import {useBookListGet, useCallAsync} from '@common/hooks';
import {UserAPI} from '@modules/users/api';

interface Props {
  id: string;
}

const UserBooksView: FC<Props> = ({ id }) => {
  const {
    books: userBooks,
    setBooks: setUserBooks,
    reloadBooks: reloadUserBooks,
    error: errorUserBooks,
    loading: loadingUserBooks
  } = useUserBooksGet(id);

  const {
    books,
    setBooks,
    reloadBooks,
    error,
    loading
  } = useBookListGet();

  const [loadingRefresh, setLoadingRefresh] = useState<boolean>();
  const callAsync = useCallAsync();

  const borrowBook = async (book: Persisted<BookSlim>) => {
    try {
      setLoadingRefresh(true);
      await callAsync(() => UserAPI.postBook(id, book.id));
      await callAsync(() => Promise.all([reloadBooks(), reloadUserBooks()]));
    } catch(e) {
      console.error(e);
    } finally {
      setLoadingRefresh(false);
    }
  }

  const returnBook = async (book: Persisted<BookSlim>) => {
    try {
      setLoadingRefresh(true);
      await callAsync(() => UserAPI.deleteBook(id, book.id));
      await callAsync(() => Promise.all([reloadBooks(), reloadUserBooks()]));
    } catch(e) {
      console.error(e);
    } finally {
      setLoadingRefresh(false);
    }
  }

  return (
    <div className="box__fluid grid lg:grid-cols-2 md:grid-cols-1 gap-x-2">
      <ViewLoader loaded={!loadingUserBooks && !loadingRefresh}>
        <h1 className="mb-0.5">My books</h1>
        {!!errorUserBooks && <p>Error!</p>}
        {userBooks && (
          <ul>
            {userBooks.map((book: Persisted<BookSlim>) => {
              const ret = () => returnBook(book);
              return (
                <li className="flex items-center" key={book.id}>
                  <div className="flex-1">
                    <BookCard showAvailable={false} book={book} />
                  </div>
                  <button className="btn ml-1" type="button" onClick={ret}>
                    Return
                  </button>
                </li>
              );
            })}
          </ul>
        )}
      </ViewLoader>
      <ViewLoader loaded={!loading && !loadingRefresh}>
        <h1 className="mb-0.5">Available books</h1>
        {!!error && <p>Error!</p>}
        {books && (
          <ul>
            {books.map((book: Persisted<BookSlim>) => {
              const borrow = () => borrowBook(book);
              return (
                <li className="flex items-center" key={book.id}>
                  <div className="flex-1">
                    <BookCard book={book} />
                  </div>
                  <button disabled={!book.available} className="btn ml-1" type="button" onClick={borrow}>
                    Borrow
                  </button>
                </li>
              );
            })}
          </ul>
        )}
      </ViewLoader>
    </div>
  );
}
export default UserBooksView;
