import {useCallAsync} from '@common/hooks';
import {Persisted, User} from '@common/models';
import ViewLoader from '@common/components/ViewLoader/ViewLoader';
import {FC} from 'react';
import {useRouter} from 'next/router';
import {useUserGet} from '@modules/users/hooks/useUserGet';
import UserForm from '@modules/users/components/UserForm/UserForm';
import {UserAPI} from '@modules/users/api';

interface Props {
  id: string;
}

const UserEdit: FC<Props> = ({ id }) => {
  const { user, loading: loadingGet, error } = useUserGet(id);
  const callAsync = useCallAsync()
  const router = useRouter();

  const onSubmit = async (user: User) => {
    try {
      const updateUser = await callAsync<Persisted<User> | undefined>(() => UserAPI.putUser(id, user));
      if (updateUser) {
        await router.replace(`/users/${updateUser.id}`);
      }
    } catch (e) {
      console.error(e);
    }
  }

  return (
    <ViewLoader loaded={!loadingGet}>
      {error && <p>Error!</p>}
      {user && (
        <div className="grid lg:grid-cols-2 md:grid-cols-1">
          <div className="card p-2">
            <UserForm user={user} onSubmit={onSubmit} />
          </div>
        </div>
      )}
    </ViewLoader>
  )
}
export default UserEdit;
