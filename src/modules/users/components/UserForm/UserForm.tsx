import {FC} from 'react';
import {Form, Formik, FormikHelpers} from 'formik';
import InputField from '@common/components/Form/InputField/InputField';
import * as yup from 'yup';
import {User} from '@common/models';

const validationSchema = yup.object().shape({
  firstName: yup.string().required().max(255),
  lastName: yup.string().required().max(255),
  dob: yup.date().required(),
});

const initialValues: User = {
  firstName: '',
  lastName: '',
  dob: '',
}

interface Props {
  onSubmit: (user: User) => void;
  user?: User;
}

const UserForm: FC<Props> = ({ onSubmit, user }) => {

  const formValues: User = {
    ...initialValues,
    ...user
  };

  const handleSubmit = async (user: User, { setSubmitting }: FormikHelpers<User>) => {
    setSubmitting(true);

    try {
      await onSubmit(user);
    } finally {
      setSubmitting(false);
    }
  }

  return (
    <Formik initialValues={formValues} onSubmit={handleSubmit} enableReinitialize={true} validationSchema={validationSchema}>
      {({ isSubmitting }) => (
        <Form>
          <InputField label="First name *" name="firstName"/>

          <InputField label="Last name *" name="lastName"/>

          <InputField label="Date of birth *" type="date" name="dob" />

          <div className="flex">
            <button className="ml-auto btn" type="submit" disabled={isSubmitting}>
              Submit
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
}
export default UserForm;
