import {Book, BookSlim} from '@common/models';
import {Persisted} from '@common/models';
import axios from 'axios';

const api = axios.create({
  baseURL: '/api/books',
  headers: { 'Content-Type': 'application/json' }
});

export const BooksAPI = {
  getBooks: async (): Promise<Persisted<BookSlim>[]> => {
    return await api.get('/').then(res => res.data);
  },
  getBook: async (id: string): Promise<Persisted<Book>> => {
    return await api.get(`/${id}`).then(res => res.data);
  },
  postBook: async (book: Book): Promise<Persisted<Book>> => {
    return await api.post('/', book).then(res => res.data);
  },
  putBook: async (id: string, book: Book): Promise<Persisted<Book>> => {
    return await api.put(`/${id}`, book).then(res => res.data);
  }
}
