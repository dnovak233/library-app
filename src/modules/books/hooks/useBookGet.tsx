import {useCallback} from 'react';
import {BooksAPI} from '@modules/books/api';
import {useAsyncData} from '@common/hooks';
import {Book, Persisted} from '@common/models';

export function useBookGet(id: string) {

  const getBook = useCallback(
    async () => BooksAPI.getBook(id),
    [id]
  );

  const { data, error, loading } = useAsyncData<Persisted<Book> | undefined>(getBook);

  return {
    loading,
    error,
    book: data,
  }
}
