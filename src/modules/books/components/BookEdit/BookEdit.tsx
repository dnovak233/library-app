import {useBookGet} from '@modules/books/hooks';
import {useCallAsync} from '@common/hooks';
import {Book, Persisted} from '@common/models';
import {BooksAPI} from '@modules/books/api';
import ViewLoader from '@common/components/ViewLoader/ViewLoader';
import BookForm from '@modules/books/components/BookForm/BookForm';
import {FC} from 'react';
import {useRouter} from 'next/router';

interface Props {
  id: string;
}

const BookEdit: FC<Props> = ({ id }) => {
  const { book, loading: loadingGet, error } = useBookGet(id);
  const callAsync = useCallAsync();
  const router = useRouter();

  const onSubmit = async (book: Book) => {
    try {
      const updatedBook = await callAsync<Persisted<Book> | undefined>(() => BooksAPI.putBook(id, book));
      if (updatedBook) {
        await router.replace(`/books/${updatedBook.id}`);
      }
    } catch (e) {
      console.error(e);
    }
  }

  return (
    <ViewLoader loaded={!loadingGet}>
      {error && <p>Error!</p>}
      {book && (
        <div className="grid lg:grid-cols-2 md:grid-cols-1">
          <div className="card p-2">
            <BookForm book={book} onSubmit={onSubmit} />
          </div>
        </div>
      )}
    </ViewLoader>
  )
}
export default BookEdit;
