import {FC, useMemo} from 'react';
import {Book} from '@common/models/Book';
import {Form, Formik, FormikHelpers} from 'formik';
import InputField from '@common/components/Form/InputField/InputField';
import * as yup from 'yup';

const validationSchema = yup.object().shape({
  title: yup.string().required().max(255),
  author: yup.string().required().max(255),
  summary: yup.string().required().max(1000),
  quantity: yup.number().required().max(100).min(1),
});

const initialValues: Book = {
  title: '',
  author: '',
  summary: '',
  quantity: 1,
};

interface Props {
  onSubmit: (book: Book) => void;
  book?: Book;
}

const BookForm: FC<Props> = ({ onSubmit, book }) => {

  const formValues: Book = {
    ...initialValues,
    ...book,
  };

  const updatedSchema = useMemo(
    () => {
      if (!book) return validationSchema;

      const minQuantity = book.quantity - (book.available || 0);
      return validationSchema.shape({
        quantity: yup.number().required().max(100).min(minQuantity),
      })
    },
    [book]
  );

  const handleSubmit = async (book: Book, { setSubmitting }: FormikHelpers<Book>) => {
    setSubmitting(true);

    try {
      await onSubmit(book);
    } finally {
      setSubmitting(false);
    }
  }

  return (
    <Formik initialValues={formValues} onSubmit={handleSubmit} enableReinitialize={true} validationSchema={updatedSchema}>
      {({ isSubmitting }) => (
        <Form>
          <InputField label="Title *" name="title"/>

          <InputField label="Author *" name="author"/>

          <InputField label="Summary *" name="summary"/>

          <InputField label="Quantity *" type="number" name="quantity"/>

          <div className="flex">
            <button className="ml-auto btn" type="submit" disabled={isSubmitting}>
              Submit
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
}
export default BookForm;
