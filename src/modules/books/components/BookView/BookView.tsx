import {FC} from 'react';
import Head from 'next/head';
import Link from 'next/link';
import {useBookGet} from '@modules/books/hooks/useBookGet';
import ViewLoader from '@common/components/ViewLoader/ViewLoader';

interface Props {
  id: string;
}

const BookView: FC<Props> = ({ id }) => {
  const { book, loading, error } = useBookGet(id);

  return (
    <>
      <Head>
        <title>{book ? `Library App | ${book.title}` : 'Library App'}</title>
        <meta name="description" content="Book details" />
      </Head>

      <ViewLoader loaded={!loading}>
        {error && <p>Error!</p>}
        {book && (
          <div>
            <div className="flex items-center">
              <h1>{book.title}</h1>
              <Link href={`/books/${book.id}/edit`}>
                <a className="ml-auto">Edit</a>
              </Link>
            </div>

            <h2>{book.author}</h2>
            <p>{book.summary}</p>
          </div>
        )}
      </ViewLoader>
    </>
  );
}
export default BookView;
