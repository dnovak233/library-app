import type { NextApiRequest, NextApiResponse } from 'next'
import {Book, Persisted, ApiError, BookSlim} from '@common/models';
import {sleep} from '@common/utils/sleep';
import {createBook, getBooks} from '@server/repository/bookRepo';


export default async function handler(req: NextApiRequest, res: NextApiResponse<Persisted<BookSlim>[] | Persisted<Book> | ApiError>) {
  const { method } = req;

  // GET
  if (method === 'GET') {
    // This is mocking DB call
    const { onlyAvailable } = req.query;

    try {
      await sleep(1000);
      const books: Persisted<BookSlim>[] = getBooks();
      return res.status(200).json(books);
    } catch (e) {
      res.status(500).send({ message: 'Server error'});
    }
  }

  // POST
  if (method === 'POST') {
    const book: Book = req.body;

    // This is mocking DB call
    try {
      await sleep(1000);
      const newBook = createBook(book)
      return res.status(200).json(newBook);
    } catch (e) {
      return res.status(500).send({ message: 'Server error'});
    }
  }

  return res.status(405).send({ message: 'Method not allowed'});
}
