import type { NextApiRequest, NextApiResponse } from 'next'
import {ApiError, Book, Persisted} from '@common/models';
import {MockDB} from '@server/db/MockDB';
import {sleep} from '@common/utils/sleep';
import {getBook, updateBook} from '@server/repository/bookRepo';


export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Persisted<Book> | ApiError>
) {
  const { method } = req;

  // GET BOOK
  if (method === 'GET') {
    const { id } = req.query;

    // This is mocking DB call
    try {
      await sleep(1000);
      const book = getBook(id as string);
      if (book) {
        return res.status(200).json(book);
      }

      return res.status(404).send({ message: 'Not found'});
    } catch (e) {
      return res.status(500).send({ message: 'Server error'});
    }
  }

  // PUT BOOK
  if (method === 'PUT') {
    const { body } = req;
    const { id } = req.query;
    const book = MockDB.books.find((book: Persisted<Book>) => book.id === id);

    if (book) {
      await sleep(1000);
      const updatedBook = updateBook(book, body);
      return res.status(200).json(updatedBook);
    }

    return res.status(404).send({ message: 'Not found'});
  }

  return res.status(405).send({ message: 'Method not allowed'});
}
