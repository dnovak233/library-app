import type { NextApiRequest, NextApiResponse } from 'next'
import {Persisted, ApiError, User} from '@common/models';
import {sleep} from '@common/utils/sleep';
import {createUser, getUsers} from '@server/repository/userRepo';


export default async function handler(req: NextApiRequest, res: NextApiResponse<Persisted<User>[] | Persisted<User> | ApiError>) {
  const { method } = req;

  // GET USER LIST
  if (method === 'GET') {
    // This is mocking DB call
    try {
      await sleep(1000);
      const users = getUsers();
      return res.status(200).json(users);
    } catch (e) {
      res.status(500).send({ message: 'Server error'});
    }
  }

  // POST USER
  if (method === 'POST') {
    const user: User = req.body;
    // This is mocking DB call
    try {
      await sleep(1000);
      const newUser = createUser(user);
      return res.status(200).json(newUser);
    } catch (e) {
      return res.status(500).send({ message: 'Server error'});
    }
  }

  return res.status(405).send({ message: 'Method not allowed'});
}
