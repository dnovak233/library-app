import type { NextApiRequest, NextApiResponse } from 'next'
import {Persisted, ApiError, User} from '@common/models';
import {MockDB} from '@server/db/MockDB';
import {sleep} from '@common/utils/sleep';
import {addUserBook, getUser, removeUserBook} from '@server/repository/userRepo';


export default async function handler(req: NextApiRequest, res: NextApiResponse<null | ApiError>) {
  const {method} = req;

  const { id, bookId } = req.query;

  // ADD BOOK TO USER
  if (method === 'POST') {
    // This is mocking DB call
    try {
      await sleep(1000);
      const sqlUser = getUser(id as string);
      if (!sqlUser) {
        return res.status(404).send({message: 'Not found'});
      }

      const success = addUserBook(sqlUser, bookId as string);
      return success ?
        res.status(204).json(null) :
        res.status(400).json({message: 'Bad request'});
    } catch (e) {
      res.status(500).send({message: 'Server error'});
    }
  }

  // REMOVE BOOK FROM USER
  if (method === 'DELETE') {
// This is mocking DB call
    try {
      await sleep(1000);
      let sqlUser = MockDB.users.find((u: Persisted<User>) => u.id === id);
      if (!sqlUser) {
        return res.status(404).send({message: 'Not found'});
      }

      const success = removeUserBook(sqlUser, bookId as string);
      return success ?
        res.status(204).json(null) :
        res.status(400).json({message: 'Bad request'});
    } catch (e) {
      res.status(500).send({message: 'Server error'});
    }
  }

  return res.status(405).send({ message: 'Method not allowed'});

}
