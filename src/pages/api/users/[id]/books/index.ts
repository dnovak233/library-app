import type { NextApiRequest, NextApiResponse } from 'next'
import {Persisted, ApiError, SqlUser,BookSlim} from '@common/models';
import {sleep} from '@common/utils/sleep';
import {getUser, getUserBooks} from '@server/repository/userRepo';


export default async function handler(req: NextApiRequest, res: NextApiResponse<Persisted<BookSlim>[] | ApiError>) {
  const {method} = req;

  if (method !== 'GET') {
    return res.status(405).send({ message: 'Method not allowed'});
  }

  // GET BOOK LIST FOR USER
  const { id } = req.query;

  // This is mocking DB call
  try {
    await sleep(1000);
    const sqlUser: SqlUser | null = getUser(id as string);
    if (!sqlUser) {
      return res.status(404).send({message: 'Not found'});
    }

    const books: Persisted<BookSlim>[] = getUserBooks(sqlUser);
    return res.status(200).json(books);
  } catch (e) {
    res.status(500).send({message: 'Server error'});
  }

  return res.status(404).send({message: 'Not found'});
}
