import type { NextApiRequest, NextApiResponse } from 'next'
import {ApiError, Persisted, SqlUser, User} from '@common/models';
import {sleep} from '@common/utils/sleep';
import {getUser, updateUser} from '@server/repository/userRepo';


export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Persisted<User> | ApiError>
) {
  const { method } = req;

  // GET USER
  if (method === 'GET') {
    const { id } = req.query;

    // This is mocking DB call
    try {
      await sleep(1000);
      const sqlUser: Persisted<SqlUser> | null = getUser(id as string);
      if (sqlUser) {
        const { id, firstName, lastName, dob } = sqlUser;
        const user: Persisted<User> = { id, firstName, lastName, dob };
        return res.status(200).json(user);
      }

      return res.status(404).send({ message: 'Not found'});
    } catch (e) {
      return res.status(500).send({ message: 'Server error'});
    }
  }

  // PUT USER
  if (method === 'PUT') {
    const { body } = req;
    const { id } = req.query;

    try {
      await sleep(1000);
      let sqlUser = getUser(id as string);
      if (!sqlUser) {
        return res.status(404).send({ message: 'Not found'});
      }

      const user = updateUser(sqlUser, body);
      return res.status(200).json(user);
    } catch (e) {
      return res.status(500).send({ message: 'Server error'});
    }
  }
  return res.status(405).send({ message: 'Method not allowed'});
}
