import {NextPage} from 'next';
import Head from 'next/head';
import Layout from '@common/components/Layout/Layout';
import {Persisted, User} from '@common/models';
import {useRouter} from 'next/router';
import {useCallAsync} from '@common/hooks';
import UserForm from '@modules/users/components/UserForm/UserForm';
import {UserAPI} from '@modules/users/api';


const UserCreatePage: NextPage = () => {
  const router = useRouter();
  const callAsync = useCallAsync();

  const onSubmit = async (user: User) => {
    try {
      const newUser = await callAsync<Persisted<User>>(() => UserAPI.postUser(user));
      if (newUser) {
        await router.replace(`/users/${newUser.id}`);
      }
    } catch (e) {
      console.error(e);
    }
  }

  return (
    <Layout>
      <Head>
        <title>Library App | Create User</title>
        <meta name="description" content="User create page" />
      </Head>

      <div className="mb-1">
        <h1>User Create</h1>
      </div>

      <div className="grid lg:grid-cols-2 md:grid-cols-1">
        <div className="card p-2">
          <UserForm onSubmit={onSubmit} />
        </div>
      </div>
    </Layout>
  );
}

export default UserCreatePage;
