import {NextPage} from 'next';
import Layout from '@common/components/Layout/Layout';
import {useRouter} from 'next/router';
import UserView from '@modules/users/components/UserView/UserView';
import UserBooksView from '@modules/users/components/UserBooksView/UserBooksView';


const UserViewPage: NextPage = () => {
  const router = useRouter();
  const { id } = router.query

  return (
    <Layout>
      {id && <UserView id={id as string} />}
      {id && <UserBooksView id={id as string} />}
    </Layout>
  );
}
export default UserViewPage;
