import {NextPage} from 'next';
import Head from 'next/head';
import Layout from '@common/components/Layout/Layout';
import {useRouter} from 'next/router';
import UserEdit from '@modules/users/components/UserEdit/UserEdit';


const UserEditPage: NextPage = () => {
  const router = useRouter();
  const { id } = router.query

  return (
    <Layout>
      <Head>
        <title>Library App | Edit User</title>
        <meta name="description" content="User edit page" />
      </Head>

      <div className="mb-1">
        <h1>User Edit</h1>
      </div>

      {id && <UserEdit id={id as string} />}
    </Layout>
  );
}
export default UserEditPage;
