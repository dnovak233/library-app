import {NextPage} from 'next';
import {useAsyncData} from '@common/hooks';
import {Persisted, User} from '@common/models';
import {UserAPI} from '@modules/users/api';
import Head from 'next/head';
import Link from 'next/link';
import Layout from '@common/components/Layout/Layout';
import ViewLoader from '@common/components/ViewLoader/ViewLoader';
import UserCard from '@modules/users/components/UserCard/UserCard';


const UserListPage: NextPage = () => {
  const { data: users, error, loading } = useAsyncData<Persisted<User>[]>(UserAPI.getUsers);

  return (
    <Layout>
      <Head>
        <title>Library App | Users</title>
        <meta name="description" content="List of users" />
      </Head>

      <div className="box__fixed flex mb-1">
        <h1 className="font-sans">Users List</h1>
        <Link href="/users/create">
          <a className="ml-auto">
            + Add user
          </a>
        </Link>
      </div>

      <ViewLoader loaded={!loading}>
        {error && <p>Error!</p>}
        {users && (
          <ul>
            {users.map((u: Persisted<User>) => {
              return (
                <li key={u.id}>
                  <UserCard user={u}/>
                </li>
              );
            })}
          </ul>
        )}
      </ViewLoader>
    </Layout>
  );
}

export default UserListPage;
