import {NextPage} from 'next';
import Layout from '@common/components/Layout/Layout';
import Head from 'next/head';
import Multiselect from '@common/components/Multiselect/Multiselect';

const Index: NextPage = () => {
  return (
    <Layout>
      <Head>
        <title>Library App | Create Book</title>
        <meta name="description" content="Book create page" />
      </Head>

      <div className="mb-1">
        <h1>Test</h1>
      </div>

      <div className="grid lg:grid-cols-2 md:grid-cols-1">
        <Multiselect />
      </div>
    </Layout>
  );
}

export default Index;
