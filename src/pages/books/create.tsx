import {NextPage} from 'next';
import Head from 'next/head';
import BookForm from '../../modules/books/components/BookForm/BookForm';
import Layout from '@common/components/Layout/Layout';
import {Book, Persisted} from '@common/models';
import {BooksAPI} from '@modules/books/api';
import {useRouter} from 'next/router';
import {useCallAsync} from '@common/hooks';


const BookCreatePage: NextPage = () => {
  const router = useRouter();
  const callAsync = useCallAsync();

  const onSubmit = async (book: Book) => {
    try {
      const newBook = await callAsync<Persisted<Book> | undefined>(() => BooksAPI.postBook(book));
      if (newBook) {
        await router.replace(`/books/${newBook.id}`);
      }
    } catch (e) {
      console.error(e);
    }
  }

  return (
    <Layout>
      <Head>
        <title>Library App | Create Book</title>
        <meta name="description" content="Book create page" />
      </Head>

      <div className="mb-1">
        <h1>Book Create</h1>
      </div>

      <div className="grid lg:grid-cols-2 md:grid-cols-1">
        <div className="card p-2">
          <BookForm onSubmit={onSubmit} />
        </div>
      </div>
    </Layout>
  );
}

export default BookCreatePage;
