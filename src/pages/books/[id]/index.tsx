import {NextPage} from 'next';
import BookView from '@modules/books/components/BookView/BookView';
import Layout from '@common/components/Layout/Layout';
import {useRouter} from 'next/router';


const BookViewPage: NextPage = () => {
  const router = useRouter();
  const { id } = router.query

  return (
    <Layout>
      {id && <BookView id={id as string} />}
    </Layout>
  );
}
export default BookViewPage;
