import {NextPage} from 'next';
import Head from 'next/head';
import Layout from '@common/components/Layout/Layout';
import {useRouter} from 'next/router';
import BookEdit from '@modules/books/components/BookEdit/BookEdit';


const BookEditPage: NextPage = () => {
  const router = useRouter();
  const { id } = router.query

  return (
    <Layout>
      <Head>
        <title>Library App | Edit Book</title>
        <meta name="description" content="Book edit page" />
      </Head>

      <div className="mb-1">
        <h1>Book Edit</h1>
      </div>

      {id && <BookEdit id={id as string} />}
    </Layout>
  );
}

export default BookEditPage;
