import {NextPage} from 'next';
import Head from 'next/head';
import Link from 'next/link';
import Layout from '@common/components/Layout/Layout';
import {useBookListGet} from '@common/hooks/books/useBookListGet';
import {ChangeEvent, useState} from 'react';
import ViewLoader from '@common/components/ViewLoader/ViewLoader';
import {BookSlim, Persisted} from '@common/models';
import BookCard from '@common/components/BookCard/BookCard';


const BookListPage: NextPage = () => {
  const [search, setSearch] = useState<string>('');
  const { books, loading, error } = useBookListGet({ search });

  const onSearch = (e: ChangeEvent<HTMLInputElement>) => {
    setSearch(e.target.value);
  }

  return (
    <Layout isHome={true}>
      <Head>
        <title>Library App | Books</title>
        <meta name="description" content="List of books" />
      </Head>

      <div className="box__fixed flex items-center mb-1">
        <h1 className="font-sans">Book List</h1>

        <div className="ml-auto">
          <Link href="/books/create">
            <a>
              + Add book
            </a>
          </Link>
          <input
            className="ml-1 p-0.5 rounded"
            placeholder="Search list..."
            value={search}
            onChange={onSearch}
            disabled={loading}
          />
        </div>

      </div>

      <ViewLoader loaded={!loading}>
        {error && <p>Error!</p>}
        {books && (
          <ul>
            {books.map((book: Persisted<BookSlim>) => {
              return (
                <li key={book.id}>
                  <BookCard book={book} canEdit={true} />
                </li>
              );
            })}
          </ul>
        )}
      </ViewLoader>
    </Layout>
  );
}

export default BookListPage;
