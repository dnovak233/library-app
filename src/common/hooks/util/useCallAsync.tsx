import {useCallback} from 'react';
import {useMounted} from '@common/hooks';

export function useCallAsync() {
  const isMounted = useMounted();

  return useCallback(
    async <T,>(cb: () => Promise<T | undefined>): Promise<T | undefined> => {

      return new Promise(async (resolve, reject) => {
        try {
          const value = await cb();
          if (isMounted()) {
            resolve(value);
          }
        } catch (e) {
          if (isMounted()) {
            reject(e);
          }
        }
      })
    },
    [isMounted]
  );
}
