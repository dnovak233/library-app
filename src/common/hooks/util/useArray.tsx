import { Dispatch, SetStateAction, useCallback, useState } from 'react';

type UseArray<T> = [T[], Dispatch<SetStateAction<T[]>>, UseArrayHelpers<T>];

type UseArrayHelpers<T> = {
  push: (item: T) => void;
  unshift: (item: T) => void;
  remove: (index: number) => void;
  set: (index: number, item: T) => void;
  reset: () => void;
  filter: (cb: (item: T) => boolean) => void;
};

export function useArray<T>(initialArray: T[] = []): UseArray<T> {
  const [array, setArray] = useState<T[]>(initialArray || []);

  const push = useCallback(
    (item: T) => { setArray(array => [...array, item]); },
    [],
  );

  const unshift = useCallback(
    (item: T) => { setArray(array => [item, ...array]); },
    [],
  );

  const remove = useCallback(
    (index) => {
      setArray((array: T[]) => {
        const newArray = [...array];
        newArray.splice(index, 1);
        return newArray;
      });
    },
    [],
  );

  const set = useCallback(
    (index: number, item?: T) => {
      setArray((array: T[]) => {
        const newArray = [...array];
        newArray.splice(index, 1, item!);
        return newArray;
      });
    },
    [],
  );

  const reset = useCallback(
    () => { setArray(initialArray || []); },
    [initialArray],
  );

  const filter = useCallback(
    (cb: (item: T) => boolean) => {
      setArray(array => [...array.filter(cb)]);
    },
    [],
  );

  return [array, setArray, { push, unshift, remove, set, reset, filter }];
}
