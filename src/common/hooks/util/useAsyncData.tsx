import {useCallback, useEffect, useState} from 'react';
import {useCallAsync} from '@common/hooks';

export function useAsyncData<T>(cb: () => Promise<T | undefined>) {
  const [loading, setLoading] = useState<boolean>(false);
  const [data, setData] = useState<T>();
  const [error, setError] = useState<unknown>();
  const callAsync = useCallAsync();

  const callbackMemo = useCallback(
    async () => {
      setLoading(true);
      setError(undefined);
      setData(undefined);

      try {
        const data = await callAsync<T>(cb);
        setData(data);
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    },
    [cb, callAsync]
  )

  const reloadData = useCallback(
    () =>  callbackMemo(),
    [callbackMemo]
  );

  useEffect(
    () => { callbackMemo(); },
    [callbackMemo]
  );

  return {
    data,
    setData,
    reloadData,
    error,
    loading,
  }
}
