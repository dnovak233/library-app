import {useArray} from '@common/hooks/util/useArray';
import {contains} from '@common/utils/array';

interface Options<T> {
  equals: (a: T, b: T) => boolean
}

const defaultOptions: Options<any> = {
  equals: (a, b) => a === b,
}

export function useMultiselect<T>(initialState: T[], options: Options<T> = defaultOptions) {
  const [items, setItems, { push, remove, unshift }] = useArray<T>(initialState);

  const isActive = (value: T): boolean => contains(value, items, options.equals);

  const select = (item: T) => {
    if (!isActive(item)) {
      push(item);
    }
  }

  const deselect = (item: T) => {
    if (isActive(item)) {
      const index = items.findIndex((arrayItem: T) => options.equals(item, arrayItem));
      remove(index);
    }
  }

  const toggle = (item: T) => {
    if (isActive(item)) {
      const index = items.findIndex((arrayItem: T) => options.equals(item, arrayItem));
      remove(index);
    } else {
      push(item);
    }
  }

  return {
    selected: items as any,
    select,
    deselect,
    toggle,
    isActive,
  }
}
