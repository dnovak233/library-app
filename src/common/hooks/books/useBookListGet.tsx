import {BookSlim, Persisted} from '@common/models';
import {BooksAPI} from '@modules/books/api';
import {useMemo} from 'react';
import {useAsyncData} from '@common/hooks';

interface Options {
  search: string;
}

const defaultOptions: Options = {
  search: '',
}

export function useBookListGet(options: Options = defaultOptions) {
  const { search } = options;

  const {
    data: books,
    setData: setBooks,
    reloadData: reloadBooks,
    error,
    loading
  } = useAsyncData<Persisted<BookSlim>[]>(BooksAPI.getBooks);

  const booksMemo = useMemo(
    () => books ? books.filter((b: BookSlim) => b.title.toLowerCase().includes(search.toLowerCase())) : [],
    [search, books]
  );

  return {
    loading,
    error,
    setBooks,
    reloadBooks,
    books: booksMemo,
  }
}
