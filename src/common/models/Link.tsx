export interface MenuLink {
  name: string;
  href: string;
}

export type Menu = MenuLink[];
