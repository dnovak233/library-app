export interface BookSlim {
  title: string;
  author: string;
  quantity: number;
  available?: number;
}

export type Book = BookSlim & {
  summary: string;
}
