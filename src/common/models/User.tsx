import {Persisted} from '@common/models/Persisted';

export interface User {
  firstName: string;
  lastName: string;
  dob: string;
}

export type SqlUser = Persisted<User> & {
  books: SqlUserBook[];
}

export interface SqlUserBook {
  bookId: string;
  quantity: number;
}
