export type Persisted<T> = T & { readonly id: string }
