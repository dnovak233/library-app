import Link from 'next/link';
import {FC} from 'react';
import {BookSlim, Persisted} from '@common/models';

interface Props {
  book: Persisted<BookSlim>
  canEdit?: boolean;
  showAvailable?: boolean;
}

const BookCard: FC<Props> = ({
  book: { id, title, author, quantity, available },
  canEdit = false,
  showAvailable = true,
}) => {
  return (
    <div className="card flex">
      <div>
        <h3>{title}</h3>
        <h4>{author}</h4>
        {showAvailable ? (
          <p>
            Available: <span className={available === 0 ? 'text-red' : ''}>{available}</span> / <strong>{quantity}</strong>
          </p>
        ): (
          <p>
            Quantity: {quantity}
          </p>
        )}
      </div>
      <div className="flex items-center ml-auto">
        <Link href={`/books/${id}`}>
          <a className="mx-0.5">View</a>
        </Link>
        {canEdit && (
          <Link href={`/books/${id}/edit`}>
            <a className="mx-0.5">Edit</a>
          </Link>
        )}
      </div>
    </div>
  );
}
export default BookCard;
