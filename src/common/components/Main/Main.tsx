import {FC} from 'react';
import {useRouter} from 'next/router';

interface Props {
  isHome?: boolean;
}

const Main: FC<Props> = ({ isHome, children }) => {
  const router = useRouter();

  return (
    <main className="box box__fluid container py-1 px-1 mx-auto">
      {!isHome && (
        <div className="box__fixed mb-0.25">
          <button className="text-green" onClick={router.back}>
            Go Back
          </button>
        </div>
      )}
      <div className="box box__fluid">
        {children}
      </div>
    </main>
  );
}
export default Main;
