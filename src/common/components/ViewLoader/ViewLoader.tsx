import {FC} from 'react';
import {CircularProgress} from '@mui/material';

interface Props {
  loaded: boolean;
}

const ViewLoader: FC<Props> = ({ loaded, children }) => {
  return (
    <div className="box box__fluid">
      {loaded ? children : (
        <div className="box__fluid flex justify-center items-center">
          <CircularProgress />
        </div>
      )}
    </div>
  );
}
export default ViewLoader;
