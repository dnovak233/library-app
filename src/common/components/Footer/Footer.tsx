import {FC} from 'react';

const Footer: FC = ({ children }) => {
  return (
    <footer className="box__fixed">
      <div className="container px-1 mx-auto">
        <div className="flex py-2 border-t border-dark-gray">
          {children}
        </div>
      </div>
    </footer>
  );
}
export default Footer;
