import {FC} from 'react';

const Header: FC = ({ children }) => {
  return (
    <header className="box__fixed bg-green py-2">
      <div className="container px-1 mx-auto">
        {children}
      </div>
    </header>
  );
}
export default Header;
