import { Listbox, Transition } from '@headlessui/react';
import {FC, Fragment} from 'react';
import classNames from 'classnames';
import {CheckIcon, SelectorIcon} from '@heroicons/react/solid';
import {useMultiselect} from '@common/hooks/util/useMultiselect';

const people = [
  { name: 'Wade Cooper' },
  { name: 'Arlene Mccoy' },
  { name: 'Devon Webb' },
  { name: 'Tom Cook' },
  { name: 'Tanya Fox' },
  { name: 'Hellen Schmidt' },
]

const Multiselect: FC = () => {
  const { selected, toggle, isActive } = useMultiselect<{ name: string }>([people[0], people[5]]);

  return (
    <Listbox value={selected} onChange={toggle}>
      <div className="relative w-32">
        <Listbox.Button className="flex items-center w-full py-1 pl-1 pr-1 text-left bg-white rounded-lg shadow-md cursor-default focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-orange-300 focus-visible:ring-offset-2 focus-visible:border-indigo-500 sm:text-sm">
          <span className="block truncate mr-auto">
            {selected.length ? selected.map(({ name }: { name: string }) => name).join(', ') : <i>{'Select people from list...'}</i>}
          </span>
          <span className="flex items-center pointer-events-none">
            <SelectorIcon className="w-1 h-1 text-gray-400" aria-hidden="true" />
          </span>
        </Listbox.Button>
        <Transition
          as={Fragment}
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <Listbox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
            {people.map((person, personIdx) => {
              const selected = isActive(person);
              return (
                <Listbox.Option
                  key={personIdx}
                  className={classNames('cursor-default select-none flex items-center py-1 pl-1 pr-1', selected ? 'text-amber-900 bg-amber-100' : 'text-gray-900')}
                  value={person}
                >
                  <>
                    <span className={classNames('block truncate mr-auto', selected ? 'font-bold' : 'font-normal')}>
                      {person.name}
                    </span>
                    {selected && (
                      <span className="flex pl-3 text-amber-600">
                        <CheckIcon className="w-1 h-1" aria-hidden="true"/>
                      </span>
                    )}
                  </>
                </Listbox.Option>
              );
            })}
          </Listbox.Options>
        </Transition>
      </div>
    </Listbox>
  );
};

export default Multiselect;
