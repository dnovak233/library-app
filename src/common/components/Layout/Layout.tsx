import {FC} from 'react';
import Head from 'next/head';
import Header from '@common/components/Header/Header';
import Footer from '@common/components/Footer/Footer';
import Main from '@common/components/Main/Main';
import Navbar from '@common/components/Navbar/Navbar';
import {MAIN_MENU} from '@common/constants/main-menu';

interface Props {
  isHome?: boolean;
}

const Layout: FC<Props> = ({ isHome, children }) => {
  return (
    <div className="box box__fluid">
      <Head>
        <title key="title">Library App</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preload" href="/fonts/TTNormsPro/TT-Norms-Pro-Regular.otf" as="font" />
        <link rel="preload" href="/fonts/TTNormsPro/TT-Norms-Pro-Medium.otf" as="font" />
        <link rel="preload" href="/fonts/TTNormsPro/TT-Norms-Pro-Bold.otf" as="font" />
      </Head>

      <Header>
        <Navbar menu={MAIN_MENU}/>
      </Header>

      <Main isHome={isHome}>
        {children}
      </Main>

      <Footer>
        <p className="ml-auto text-dark-gray">Dan Novak 2022</p>
      </Footer>
    </div>
  );
}
export default Layout;

