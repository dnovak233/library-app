import {FC} from 'react';

interface Props {
  message: string;
}

const InputError: FC<Props> = ({ message }) => {
  return (
    <div className="text-red text-sm my-0.25">
      <p>{message}</p>
    </div>
  );
}

export default InputError;
