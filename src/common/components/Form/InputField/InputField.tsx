import {FC} from 'react';
import {TextField} from '@mui/material';
import {useField} from 'formik';
import InputError from '@common/components/Form/InputError/InputError';

interface Props {
  label: string;
  name: string;
  type?: 'text' | 'number' | 'date';
}

const InputField: FC<Props> = ({ label, name, type = 'text' }) => {
  const [field, meta, helpers] = useField(name);
  const { onChange } = field;
  const { value, touched, error } = meta;

  return (
    <div className="mb-2">
      <TextField
        onChange={onChange}
        value={value}
        label={label}
        name={name}
        type={type}
        variant="outlined"
        fullWidth={true}
        InputLabelProps={{
          shrink: true,
        }}
        error={touched && !!error}
        autoComplete="off"
      />

      {touched && error && (
        <InputError message={error} />
      )}
    </div>
  );
};
export default InputField;
