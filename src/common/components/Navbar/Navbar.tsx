import Link from 'next/link';
import {Menu, MenuLink} from '@common/models/Link';
import {FC} from 'react';

interface Props {
  menu: Menu;
}

const Navbar: FC<Props> = ({ menu }) => {
  return (
    <nav>
      <ul className="flex">
        {menu.map((ml: MenuLink, index: number) => {
          return (
            <li key={index} className="mr-1">
              <Link href={ml.href}>
                <a className="text-white uppercase">
                  <strong>{ml.name}</strong>
                </a>
              </Link>
            </li>
          );
        })}
      </ul>
    </nav>
  )
}
export default Navbar;
