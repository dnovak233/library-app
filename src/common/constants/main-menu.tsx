import {Menu} from '@common/models/Link';

export const MAIN_MENU: Menu = [
  { href: '/books', name: 'Books' },
  { href: '/users', name: 'Users' },
  { href: '/test', name: 'Test' },
]
