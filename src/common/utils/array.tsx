export function contains<T>(item: T, array: T[], cb: (a: T, b: T) => boolean): boolean {
  for (let i = 0; i < array.length; i++) {
    if (cb(array[i], item)) {
      return true;
    }
  }
  return false;
}
